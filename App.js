import React, {Component} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Login from './src/component/loginView';
import Dashboard from './src/component/dashboardView';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
      <Text> hello</Text>
        <Login/>
        <Dashboard/>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
