import React, { Component } from 'react';
import { AppRegistry, View, Text, StyleSheet, TouchableHighlight, Alert } from 'react-native';

export default class Login extends Component{
_onPressButton() {
  Alert.alert('You tapped the button!')
}

render(){
  return(
    <View >
      <TouchableHighlight onPress={this._onPressButton} style = {styles.button}>
        <Text>Login</Text>
      </TouchableHighlight>
     </View>
  );
}
}


const styles = StyleSheet.create({
button:{
  backgroundColor:'red',
  color: 'white',
},
});
