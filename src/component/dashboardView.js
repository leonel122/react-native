import React, {Component } from 'react';
import {AppRegistry, StyleSheet, View, Text } from 'react-native';

export default class Dashboard extends Component{
  render(){
    return(
      <View>
        <Text style = {styles.texto}>Hello Dashboard</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  texto:{
    color:'red',
  },
});
